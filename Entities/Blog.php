<?php

namespace Modules\Blog\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [];

    public function User(){
        $this->hasOne(User::class,'id','user_id');
    }
}
